export interface User {
    id?: string;
    secret?: string;
    name?: string;
    wishList?: string;
    isDrawn?: boolean;
    hasDrawn?: boolean;
}
