import { Component, Output, Input, SimpleChanges } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Helpers } from '../helpers/helpers';
import { User } from '../model/user';
import { UserRepository } from '../store/userRepository';
import { ConfigurationRepository } from '../store/configurationRepository';
import { DrawConfiguration } from '../model/configuration/drawConfiguration';
import { UserHelpers } from '../helpers/userHelpers';

@Component({
  selector: 'app-draw-component',
  templateUrl: './draw-component.component.html',
  styleUrls: ['./draw-component.component.css'],
})
export class DrawComponentComponent {
  private _userRepository: UserRepository;

  private _configurationRepository: ConfigurationRepository;

  @Input() drawingUserName: string = '';
  @Output() drawnUserName: string = '';
  @Output() drawnUserNameUpper: string = '';
  @Output() secretKey: string = '';
  @Output() userAlreadyHasDrawn: boolean = false;

  ngOnChanges(changes: SimpleChanges) {
    //used to allow re-using the page for more than one user without manual refresh
    //changes object can be used to track the changes (old <> new, etc.)

    this.drawnUserName = '';
    this.drawnUserNameUpper = '';
    this.secretKey = '';
    this.userAlreadyHasDrawn = false;
  }

  constructor(db: Firestore) {
    this._userRepository = new UserRepository(db);
    this._configurationRepository = new ConfigurationRepository(db);
  }

  async draw() {
    var users: User[] = Helpers.convertObservableToArray(
      this._userRepository.user$
    );
    var drawConfig: DrawConfiguration =
      await this._configurationRepository.retrieveDrawConfiguration();

    if (await this._userRepository.hasUserDrawnAlready(this.drawingUserName)) {
      this.userAlreadyHasDrawn = true;
      return;
    }

    var drawnUser = drawConfig.canDrawPartner
      ? this.drawRandomUser(users)
      : this.drawRandomNonPartnerUser(users, this.drawingUserName);

    this.drawnUserName = drawnUser.name ?? 'Coś poszło nie tak...';
    this.drawnUserNameUpper = Helpers.stringToUpperCaseFirstLetter(
      this.drawnUserName
    );
    this.secretKey = drawnUser.secret ?? '';

    this.updateDrawnUserFields(this.drawnUserName);
    this.updateDrawingUserFields(this.drawingUserName);
  }

  updateDrawnUserFields(userName: string) {
    this._userRepository.setUserField(userName, { isDrawn: true });
  }

  updateDrawingUserFields(userName: string) {
    this._userRepository.setUserField(userName, { hasDrawn: true });
  }

  drawRandomUser(users: User[]): User {
    var i = 0;
    var max = 10000;
    var random = (max = users.length) => Math.floor(Math.random() * max);

    while (i < max) {
      var drawnUser = users[random()];
      if (!(drawnUser.isDrawn || drawnUser.name === this.drawingUserName))
        return drawnUser;
      i++;
    }

    return {};
  }

  drawRandomNonPartnerUser(users: User[], drawingUserName: string): User {
    var drawingUserPartnerUserName = UserHelpers.getUserPartnerName(drawingUserName);
    var usersWithoutDrawingUserPartner = users.filter((u) => u.name !== drawingUserPartnerUserName);
    return this.drawRandomUser(usersWithoutDrawingUserPartner);
  }
}
