import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SuccessPopupComponent } from './success-popup/success-popup.component';
import { Firestore } from '@angular/fire/firestore';
import { UserRepository } from './store/userRepository';
import { Helpers } from './helpers/helpers';
import { User } from './model/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  private _userRepository: UserRepository;

  currentUserName: string = '';
  userData: any = {};

  constructor(public dialog: MatDialog, db: Firestore) {
    this._userRepository = new UserRepository(db);
  }

  async sendData(formData: NgForm) {
    var dbUser = await this._userRepository.getUserByName(this.currentUserName);

    this.userData = formData;

    this.userData.name = this.currentUserName;
    this.userData.isDrawn = dbUser.isDrawn;
    this.userData.hasDrawn = dbUser.hasDrawn;
    this.userData.secret = dbUser.secret;
    this.userData.wishList = this.userData.wishList;

    if (this.currentUserName !== '' && this.userData.wishList !== '') {
      await this._userRepository.addUpdateUser(this.userData);
      this.openDialogSuccess();
    }
  }

  openDialogSuccess() {
    this.dialog.open(SuccessPopupComponent);
  }

  async clearDrawingData() {
    var users: User[] = this._userRepository.users;
    var uniqueUserNames = [...new Set(users.map((u) => u.name))];

    console.warn('Data reset start...');
    for (const userName of uniqueUserNames) {
      await this._userRepository.setUserField(userName ?? '', {
        isDrawn: false,
      });
      await this._userRepository.setUserField(userName ?? '', {
        hasDrawn: false,
      });
      await this._userRepository.setUserField(userName ?? '', {
        wishList: '',
      });
      console.warn(userName);
    }
    console.warn('...finished data reset.');
  }
}