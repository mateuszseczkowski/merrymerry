export interface SuccessPopup {
    title: string;
    content: string;
}
