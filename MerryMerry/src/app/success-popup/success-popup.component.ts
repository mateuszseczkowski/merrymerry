import { Component } from '@angular/core';
import { SuccessPopup } from './success-popup';

@Component({
  selector: 'app-success-popup',
  templateUrl: './success-popup.component.html',
})
export class SuccessPopupComponent {
  successPopup: SuccessPopup = { title: "Wysłano.", content: "Wesołych Świąt!" };
}
