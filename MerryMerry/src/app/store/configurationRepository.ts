import {
  Firestore,
  doc,
  getDoc,
} from '@angular/fire/firestore';
import { DrawConfiguration } from '../model/configuration/drawConfiguration';

export class ConfigurationRepository {
  private _client: Firestore;

  constructor(client: Firestore) {
    this._client = client;
  }

  async retrieveDrawConfiguration(): Promise<DrawConfiguration> {
    var drawingConfiguration =
      (await getDoc(doc(this._client, 'configs', 'draw'))).data() ?? {};
    return drawingConfiguration;
  }
}
