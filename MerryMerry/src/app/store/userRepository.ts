import {
    Firestore, collectionData, collection,
    doc, getDoc, getDocs, query, where,
    setDoc, updateDoc
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { Helpers } from '../helpers/helpers'

export class UserRepository {
    private _client: Firestore;

    user$: Observable<User[]>;
    users: User[];

    constructor(client: Firestore) {
        this._client = client;
        this.user$ = this.retrieveUsersFromDB();
        this.users = Helpers.convertObservableToArray(this.user$);
    }

    retrieveUsersFromDB() {
        return collectionData(collection(this._client, 'users'));
    }

    async addUpdateUser(user: any) {
        await setDoc(doc(this._client, "users", user.name), user);
    }

    async getUserByName(name: string) : Promise<User> {
        var user =
          (await getDoc(doc(this._client, 'users', name))).data() ?? {};
        return user;
    }

    async getUserBySecret(secret: string) : Promise<User> {
        var users: User[] = [];

        const q = query(collection(this._client, "users"), where("secret", "==", secret));
        const querySnapshot = await getDocs(q);

        querySnapshot.forEach((doc) => {
            users.push(doc.data());
        });

        return users[0];
    }

    async setUserField(userName: string, data: {[x: string]: any}) {
        const userReference = doc(this._client, "users", userName);

        await updateDoc(userReference, data);
    }

    async hasUserDrawnAlready(userName: string): Promise<boolean> {
        const user = await this.getUserByName(userName);

        return user.hasDrawn ?? false;
    }
}