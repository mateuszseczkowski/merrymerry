import { UsersEnum } from '../enums.ts/users.enum';

export class UserHelpers {
  private static separator = ',';
  private static partners: string[] = [
    UsersEnum.karolina + this.separator + UsersEnum.mateusz,
    UsersEnum.ania + this.separator + UsersEnum.lukasz,
    UsersEnum.iza + this.separator + UsersEnum.radek,
    UsersEnum.ilona + this.separator + UsersEnum.robert,
  ];

  static getUserPartnerName(userName: string): string {
    var couple = this.partners
      .find((c) => c.includes(userName))
      ?.split(this.separator);

    var partnerName = couple?.find((u) => !u.includes(userName));
    return partnerName ?? '';
  }
}
