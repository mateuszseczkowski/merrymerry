import { Observable } from "rxjs";

export class Helpers {
    static convertObservableToArray(observable: Observable<any[]>): any[] {
        var array: any[] = [];

        observable.forEach(value => {
          value.forEach((user) => {
            array.push(user);
          });
        });

        return array;
    }

    static stringToUpperCaseFirstLetter(str: string) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}