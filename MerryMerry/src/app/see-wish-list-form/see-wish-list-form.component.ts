import { Component } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { NgForm } from '@angular/forms';
import { UserRepository } from '../store/userRepository';
import { Helpers } from '../helpers/helpers';

import linkifyString from "linkify-string";

@Component({
  selector: 'app-see-wish-list-form',
  templateUrl: './see-wish-list-form.component.html'
})
export class SeeWishListFormComponent {
  private _userRepository: UserRepository;
  secretKey: any = {};
  wishes: string = "";
  userWithWishesName: string = "";

  constructor(db: Firestore) {
    this._userRepository = new UserRepository(db);
  }

  async getUserWishes(formData: NgForm) {
    this.secretKey = formData.value;

    if (this.secretKey.secretKey.length < 0) return;

    var user = await this._userRepository.getUserBySecret(
      this.secretKey.secretKey
    );

    if (user == null || user.name == null || user.wishList == null) {
      this.userWithWishesName = "";
      this.wishes = "Błędny klucz!";
      return;
    }

    this.userWithWishesName = Helpers.stringToUpperCaseFirstLetter(user.name);
    this.wishes = user.wishList == "" ? "Jeszcze pusto. Spróbuj ponownie później :)" : linkifyString(user.wishList, {target: "_blank", truncate: 80});
  }
}