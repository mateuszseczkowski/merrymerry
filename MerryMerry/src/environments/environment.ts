// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'merrymerry-a4bd1',
    appId: '1:1068388754180:web:ecea072ba5786766162651',
    storageBucket: 'merrymerry-a4bd1.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyCEqSVmShmvjsJhCaSGI5IIy-ELw-24q-k',
    authDomain: 'merrymerry-a4bd1.firebaseapp.com',
    messagingSenderId: '1068388754180',
    measurementId: 'G-SY9WRZSTBV',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
